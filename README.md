
created project

`dotnet new webapi --name sample-api`


Setup a proxy for the following nuget feeds in my local nexus server (repo.proto.tsfrt.net):

https://pkgs.dev.azure.com/dotnet/Steeltoe/_packaging/dev/nuget/v3/index.json

https://api.nuget.org/v3/index.json

```

dotnet restore -v d

Feeds used:
             http://repo.proto.tsfrt.net/repository/toms-nuget/index.json
             http://repo.proto.tsfrt.net/repository/steeltoe-proxy/index.json

```

created TBS image

```

kp image create dotnet9 \
--tag "harbor.proto.tsfrt.net/dotnet/dotnet-example9" \
--git "https://gitlab.com/tanzu-e2e/dotnet-api.git" \
--git-revision master \
--cluster-builder default -w

```


Output from TBS

```

Creating Image...
Image "dotnet9" created
===> PREPARE
Build reason(s): CONFIG
CONFIG:
        resources: {}
        - source: {}
        + source:
        +   git:
        +     revision: main
        +     url: https://gitlab.com/tanzu-e2e/dotnet-api.git
Loading secret for "harbor.proto.tsfrt.net" from secret "harbor" at location "/var/build-secrets/harbor"
Cloning "https://gitlab.com/tanzu-e2e/dotnet-api.git" @ "main"...
resolving main: reference not found
Error: update to image failed
tseufert-a01:sample-api tseufert$ kp image create dotnet9 --tag "harbor.proto.tsfrt.net/dotnet/dotnet-example9" --git "https://gitlab.com/tanzu-e2e/dotnet-api.git" --git-revision master --cluster-builder default -w
Creating Image...
Error: images.kpack.io "dotnet9" already exists
tseufert-a01:sample-api tseufert$ kp image patch dotnet9 --tag "harbor.proto.tsfrt.net/dotnet/dotnet-example9" --git "https://gitlab.com/tanzu-e2e/dotnet-api.git" --git-revision master --cluster-builder default -w
Error: unknown flag: --tag
tseufert-a01:sample-api tseufert$ kp image save dotnet9 --tag "harbor.proto.tsfrt.net/dotnet/dotnet-example9" --git "https://gitlab.com/tanzu-e2e/dotnet-api.git" --git-revision master --cluster-builder default -w
Patching Image...
Image "dotnet9" patched
===> PREPARE
Build reason(s): COMMIT
COMMIT:
        - main
        + cc4307c3691ecb30d91a9fdb070bab0855efac9e
Loading secret for "harbor.proto.tsfrt.net" from secret "harbor" at location "/var/build-secrets/harbor"
Cloning "https://gitlab.com/tanzu-e2e/dotnet-api.git" @ "cc4307c3691ecb30d91a9fdb070bab0855efac9e"...
Successfully cloned "https://gitlab.com/tanzu-e2e/dotnet-api.git" @ "cc4307c3691ecb30d91a9fdb070bab0855efac9e" in path "/workspace"
===> DETECT
6 of 7 buildpacks participating
tanzu-buildpacks/icu                 0.0.9
tanzu-buildpacks/dotnet-core-runtime 0.0.30
tanzu-buildpacks/dotnet-core-aspnet  0.0.24
tanzu-buildpacks/dotnet-core-sdk     0.0.22
tanzu-buildpacks/dotnet-publish      0.0.13
tanzu-buildpacks/dotnet-execute      0.0.28
===> ANALYZE
Previous image with name "harbor.proto.tsfrt.net/dotnet/dotnet-example9" not found
===> RESTORE
===> BUILD
Tanzu ICU Buildpack 0.0.9
  Executing build process
    Installing ICU
      Completed in 1.891s


Tanzu .NETCore Runtime Buildpack 0.0.30
   3.1.10: Contributing to layer
    Reusing cached download from buildpack
    Expanding to /layers/tanzu-buildpacks_dotnet-core-runtime/dotnet-runtime
    Writing DOTNET_ROOT to shared
    Writing RUNTIME_VERSION to build

Tanzu ASPNetCore Buildpack 0.0.24
   3.1.10: Contributing to layer
    Reusing cached download from buildpack
    Expanding to /layers/tanzu-buildpacks_dotnet-core-aspnet/dotnet-aspnetcore
  Tanzu ASPNetCore Buildpack 0.0.24: Contributing to layer
    Writing DOTNET_ROOT to shared

Tanzu .NET Core SDK Buildpack 0.0.22
   3.1.404: Contributing to layer
    Reusing cached download from buildpack
    Expanding to /layers/tanzu-buildpacks_dotnet-core-sdk/dotnet-sdk
    Writing SDK_LOCATION to build
  Tanzu .NET Core SDK Buildpack 0.0.22: Contributing to layer
    Symlinking runtime libraries
    Moving dotnet driver from /layers/tanzu-buildpacks_dotnet-core-sdk/dotnet-sdk
    Writing PATH to shared
    Writing DOTNET_ROOT to shared
Tanzu .Net Publish Buildpack 0.0.13
  Executing build process
    Running 'dotnet publish /workspace --configuration Release --runtime ubuntu.18.04-x64 --self-contained false --output /workspace'
      Completed in 11.405850324s

Tanzu .NET Execute Buildpack 0.0.28
  Assigning launch processes
    web: ./sample-api --urls http://0.0.0.0:${PORT:-8080}

===> EXPORT
Adding layer 'tanzu-buildpacks/icu:icu'
Adding layer 'tanzu-buildpacks/dotnet-core-runtime:dotnet-runtime'
Adding layer 'tanzu-buildpacks/dotnet-core-aspnet:aspnet-symlinks'
Adding layer 'tanzu-buildpacks/dotnet-core-aspnet:dotnet-aspnetcore'
Adding layer 'tanzu-buildpacks/dotnet-core-sdk:driver-symlinks'
Adding 1/1 app layer(s)
Adding layer 'launcher'
Adding layer 'config'
Adding layer 'process-types'
Adding label 'io.buildpacks.lifecycle.metadata'
Adding label 'io.buildpacks.build.metadata'
Adding label 'io.buildpacks.project.metadata'
Setting default process type 'web'
*** Images (sha256:590d7581324c8124ac76b88ec762de7d826fb996646bb54123daf167a7e36979):
      harbor.proto.tsfrt.net/dotnet/dotnet-example9
      harbor.proto.tsfrt.net/dotnet/dotnet-example9:b2.20210520.132901
Adding cache layer 'tanzu-buildpacks/icu:icu'
===> COMPLETION
Build successful
tseufert-a01:sample-api tseufert$ kp image create dotnet9 --tag "harbor.proto.tsfrt.net/dotnet/dotnet-example9" --git "https://gitlab.com/tanzu-e2e/dotnet-api.git" --git-revision master --cluster-builder default -w
tseufert-a01:sample-api tseufert$ dotnet restore -v d
Build started 5/20/2021 9:31:44 AM.

```

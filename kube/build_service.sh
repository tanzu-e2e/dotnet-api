#!/bin/bash -x

. kube/kubeconfig.sh $build_cluster_context

IMAGE_NAME="$CI_PROJECT_NAME-${current_version}"
REGISTRY_IMAGE="${EXT_REGISTRY_IMAGE:-$CI_REGISTRY_IMAGE}"

kp secret list

kp image save "$IMAGE_NAME" \
--tag "$REGISTRY_IMAGE/$IMAGE_NAME" \
--local-path "./" \
--namespace tbs-images \
--cluster-builder default -w

if [ ! -z ${sign_image} ]
then
  cp ${notary_patch} patch.yaml
  cp ${notary_values} values.yaml

  ytt -f patch.yaml \
  -f values.yaml \
  -v notary.secret=${notary_secret} \
  -v notary.url=${notary_url} > patch_value.yaml

  kubectl patch image -n tbs-images $IMAGE_NAME --patch "$(cat patch_value.yaml)" --type merge
fi

latest_image=$(kubectl get image -n default $IMAGE_NAME -o json | jq .status.latestImage)
echo "image_reference=${latest_image}" >> image.env
